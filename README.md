# [enqueue](https://phppackages.org/s/enqueue)/[enqueue-bundle](https://phppackages.org/p/enqueue/enqueue-bundle)

[**enqueue/enqueue-bundle**](https://packagist.org/packages/enqueue/enqueue-bundle)
[![PHPPackages Rank](https://pages-proxy.gitlab.io/phppackages.org/enqueue/enqueue-bundle/rank.svg)](http://phppackages.org/p/enqueue/enqueue-bundle)
Message queue bundle for Symfony. RabbitMQ, Amazon SQS, Redis, Service bus, Async events, RPC over MQ and a lot more [enqueue.forma-pro.com](https://enqueue.forma-pro.com)

## [Flex Recipe](https://symfony.sh/)
* https://github.com/symfony/recipes-contrib/tree/master/enqueue/enqueue-bundle
